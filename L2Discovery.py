from nornir import InitNornir
from nornir_netmiko.tasks import netmiko_send_command
from nornir_utils.plugins.functions import print_result
from pprint import pprint
from nornir.core.task import Task, Result
from nornir_netmiko.connections import CONNECTION_NAME
from genie.libs.parser.iosxe.show_cdp import ShowCdpNeighborsDetail
from genie.libs.parser.iosxe.show_vlan import ShowVlan
from unittest.mock import Mock
import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout
import warnings
warnings.filterwarnings('ignore')
import requests

nr = InitNornir()

# pprint(nr.inventory.hosts)
getCDPNeighborsData = nr.run(
    name="get CDP neighbors data",
    task= netmiko_send_command,
    command_string = "show cdp neighbors detail"
)
getVLANData = nr.run(
    name="get VLAN data",
    task= netmiko_send_command,
    command_string = "show vlan"
)


getCDPNeighbors_genie = nr.run(
    name="get CDP neighbors data",
    task= netmiko_send_command,
    command_string = "show cdp neighbors detail",
    use_genie = True
)

# print_result(getCDPNeighbors_genie)
# print_result(getVLANData)


def CustomTask(task: Task , variable) -> Result:
    return Result(
        host= task.host,
        result = f"Hostname : {task.host}\nPlatform: {task.host.platform}\nCustom variable: {variable}"
    )

customTaskResult = nr.run(
    name="Custom Task",
    task= CustomTask,
    variable = "This is a Custom Var"
)
# print_result(customTaskResult)
def customCDPParser(task: Task) -> Result :
    if(task.host.platform == 'ios'):
        net_connect = task.host.get_connection(CONNECTION_NAME, task.nornir.config)
        result = net_connect.send_command("show cdp neighbors detail")
        parser = ShowCdpNeighborsDetail(Mock())
        result = parser.cli(result)
    return Result(
        host= task.host,
        result= result
    )

def customVlanParser(task: Task) -> Result :
    if(task.host.platform == 'ios'):
        net_connect = task.host.get_connection(CONNECTION_NAME, task.nornir.config)
        result = net_connect.send_command("show vlan")
        parser = ShowVlan(Mock())
        result = parser.cli(result)
    return Result(
        host= task.host,
        result= result
    )

customCDPResult = nr.run(
    name="Get CDP Neighbors",
    task= customCDPParser
)
customVlanResult = nr.run(
    name="Get Vlans",
    task= customVlanParser
)

# print_result(customCDPResult)
# print_result(customVlanResult)

nodes = list(customCDPResult.keys())
# print(nodes)

G = nx.MultiGraph()
G.add_nodes_from(nodes)
# print(G.nodes)
# print(G.nodes['SW1'])
# G.nodes['SW1']['Custom_Data'] = "Some Data"
# G.nodes['SW1']
# del G.nodes['SW1']['Custom_Data']
# G.nodes['SW1']
# G.nodes['SW1']
# print(customVlanResult['SW6'].result)
for node in G.nodes:
    node_vlanDB = []
    for vlan in customVlanResult[node].result['vlans']:
        vlan_data = customVlanResult[node].result['vlans'][vlan]
        if(vlan_data['type'] == 'enet'):
            node_vlanDB.append(vlan)
    G.nodes[node]['vlans'] = node_vlanDB


G.nodes.data()
G.add_edge('SW1','SW2')
G.edges
G.add_edge('SW1','SW2' , data="Some Data")
G.edges
G.edges[(('SW1', 'SW2', 0))]
G.edges[(('SW1', 'SW2', 1))]
G.edges[(('SW2', 'SW1', 1))]
G.get_edge_data('SW1','SW2')
G.clear_edges()
# pprint(customCDPResult['SW4'].result)
# pprint(customCDPResult['SW4'].result['index'])
# pprint(customCDPResult['SW4'].result['index'][1])
# pprint(customCDPResult['SW5'].result['index'][3])

for node in G.nodes:
    neighbors = customCDPResult[node].result['index']
    for nei_ix in neighbors:
        neighbor = customCDPResult[node].result['index'][nei_ix]
        data = {}
        data[node] = {
            'local_interface' : neighbor['local_interface'],
            'remote_interface': neighbor['port_id'],
            'remote_device' : neighbor['device_id'].split(".")[0],
        }
        data[neighbor['device_id'].split(".")[0]] = {
            'local_interface' : neighbor['port_id'],
            'remote_interface': neighbor['local_interface'],
            'remote_device' : node,
        }
        if(G.has_edge(node , neighbor['device_id'].split(".")[0] )):
            nodes_data = G.get_edge_data(node , neighbor['device_id'].split(".")[0])
            mapped = False
            for data in nodes_data:
                edge_data = nodes_data[data]['data']
                if(edge_data[node]['local_interface'] == neighbor['local_interface']):
                    mapped = True
            if(not mapped):
                G.add_edge(node, neighbor['device_id'].split(".")[0] , data=data )
        else:
            G.add_edge(node, neighbor['device_id'].split(".")[0] , data=data )

# pprint(G.edges())
# pprint(G.edges.data())
# pprint(G.edges[('SW1', 'SW3',0)]['data']['SW1'])
# pprint(G.edges[('SW1', 'SW3',0)]['data']['SW3'])

vlan10Nodes = []
for node in G.nodes:
    if '10' in G.nodes[node]['vlans']:
        vlan10Nodes.append(node)

# pprint(vlan10Nodes)
vlan10Graph = nx.subgraph(G , vlan10Nodes)
vlan10Graph.nodes
vlan10Graph.edges

plt.figure(figsize=(5, 5))
nx.draw(G, with_labels=True)

plt.figure(figsize=(5, 5))
nx.draw(vlan10Graph, with_labels=True)

# pprint(list(nx.connected_components(vlan10Graph)))
# pprint(len(list(nx.connected_components(vlan10Graph))))
# pprint(list(nx.all_simple_paths(G, source='SW6', target='SW2')))

for path in nx.all_simple_paths(G, source='SW6', target='SW2'):
    for node in path:
        if( not vlan10Graph.has_node(node)):
            print(f"Node {node} without VLAN configuration")

api_token = "bf3ae6ca6ead5bedb95836b7eb633997efc5a9d3"
headers = {
        'accept': 'application/json',
        'Authorization': f"Token {api_token}",
    }

url = 'http://192.168.20.148:8000/api'
endpoint = '/dcim/platforms/'
URI = f"{url}{endpoint}"
platform_params = {
        'name': 'ios',
}
r = requests.get(URI, headers=headers, params=platform_params, verify=False)
platform_response = r.json()


pprint(platform_response)

def CreateDevice(device_name, device_type , device_role , platform , site ):
    api_token = "bf3ae6ca6ead5bedb95836b7eb633997efc5a9d3"
    headers = {
        'accept': 'application/json',
        'Authorization': f"Token {api_token}",
    }
    
    url = 'http://192.168.20.148:8000/api'
    endpoint = '/dcim/platforms/'
    URI = f"{url}{endpoint}"
    platform_params = {
        'name': platform,
    }
    r = requests.get(URI, headers=headers, params=platform_params, verify=False)
    platform_response = r.json()
    platform_id = platform_response['results'][0]['id']
    
    endpoint = '/dcim/sites/'
    URI = f"{url}{endpoint}"
    sites_params = {
        'name': site,
    }
    r = requests.get(URI, headers=headers, params=sites_params, verify=False)
    site_response = r.json()
    site_id = site_response['results'][0]['id']
    # print(f"xxxxxxx{site_response}")
    
    endpoint = '/dcim/device-roles/'
    URI = f"{url}{endpoint}"
    role_params = {
        'name': device_role,
    }
    r = requests.get(URI, headers=headers, params=role_params, verify=False)
    role_response = r.json()
    role_id = role_response['results'][0]['id']
    
    endpoint = '/dcim/device-types/'
    URI = f"{url}{endpoint}"
    type_params = {
        'model': device_type,
    }
    r = requests.get(URI, headers=headers, params=type_params, verify=False)
    type_response = r.json()
    type_id = type_response['results'][0]['id']
    
    endpoint = '/dcim/devices/'
    URI = f"{url}{endpoint}"
    device_payload = {
        "name": device_name,
        "device_type": type_id,
        "device_role": role_id,
        "platform":  platform_id,
        "site": site_id
    }
    r = requests.post(URI, headers=headers, json=device_payload, verify=False)
    return r.status_code

for node in G.nodes:
    device_type = "IOSv"
    device_role = "DC"
    platform =  "ios"
    site = 'DataCenter'
    CreateDevice(node,  device_type , device_role , platform , site )

def connectDevices(localDevice , localInterface , remoteDevice, remoteInterface):
    api_token = "bf3ae6ca6ead5bedb95836b7eb633997efc5a9d3"
    headers = {
        'accept': 'application/json',
        'Authorization': f"Token {api_token}",
    }
    termination_type = "dcim.interface"
    url = 'http://192.168.20.148:8000/api'
    endpoint = '/dcim/interfaces/'
    URI = f"{url}{endpoint}"
    localData = {
        'name': localInterface,
        'device' : localDevice
    }
    r = requests.get(URI, headers=headers, params=localData, verify=False)
    localResponse = r.json()
    localId = localResponse['results'][0]['id']
    remoteData = {
        'name': remoteInterface,
        'device' : remoteDevice
    }
    r = requests.get(URI, headers=headers, params=remoteData, verify=False)
    remoteResponse = r.json()
    remoteId = remoteResponse['results'][0]['id']
    payload = {
        "termination_a_type": termination_type,
        "termination_a_id": localId,
        "termination_b_type": termination_type,
        "termination_b_id": remoteId,
    }
    endpoint = '/dcim/cables/'
    URI = f"{url}{endpoint}"
    r = requests.post(URI, headers=headers, json=payload, verify=False)
    return r.status_code

for edge in G.edges:
    localDevice = edge[0]
    localInterface = G.edges[edge]['data'][edge[0]]['local_interface']
    remoteDevice = G.edges[edge]['data'][edge[0]]['remote_device']
    remoteInterface = G.edges[edge]['data'][edge[0]]['remote_interface']
    connectDevices(localDevice , localInterface , remoteDevice, remoteInterface)

